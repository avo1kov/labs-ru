const a = document.querySelectorAll('nav a');
console.log(a);

for (let i = 0; i < a.length; i++) {
    a[i].addEventListener('click', function(event) {
        event.preventDefault(); // abort <a> click
        const element = document.querySelector(a[i].getAttribute('href')); // get the signed element
        console.log(a[i].getAttribute('href'));
        console.log(element);

        const scrollYStart = window.scrollY;
        let elementOffsetTop = getOffset(element).top - 20;
        if (elementOffsetTop > document.body.offsetHeight - window.innerHeight) {
            elementOffsetTop = document.body.offsetHeight  - window.innerHeight;
        }
        const scrollOffset = elementOffsetTop - scrollYStart;

        console.log(scrollOffset, elementOffsetTop, document.body.offsetHeight - window.innerHeight);
        if (window.scrollY !== elementOffsetTop) {
            animate({
                duration: 500, // sets same scrolling speed for different scroll offset (change to constant for same scrolling time)
                timing: easeInOutQuad,
                draw: function(progress) {
                    window.scrollTo(0, scrollYStart + progress * scrollOffset);
                }
            });
        }
    });
}

function animate(options) {

    let start = performance.now();

    requestAnimationFrame(function animate(time) {
        // timeFraction from 0 to 1
        let timeFraction = (time - start) / options.duration;
        if (timeFraction > 1) timeFraction = 1;

        // current animate state
        let progress = options.timing(timeFraction);

        options.draw(progress);

        if (timeFraction < 1) {
            requestAnimationFrame(animate);
        }

    });
}

function easeInOutQuad(t) {
    return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1
}

function getOffset( el ) {
    let _x = 0;
    let _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}