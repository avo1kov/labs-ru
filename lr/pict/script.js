const pictures = [
    'src/1.jpg',
    'src/2.jpg',
    'src/3.jpg',
    'src/4.jpg',
];
let currentPicture = -1;
const $block = document.getElementById('block');

nextPicture = () => {
    currentPicture++;
    if (currentPicture >= pictures.length) {
        currentPicture = 0;
    }
    $block.style.backgroundImage = `url(${pictures[currentPicture]})`;
};
nextPicture();

$block.addEventListener('mouseleave', () => {
    nextPicture();
});