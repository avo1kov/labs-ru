$(function(){
    var leftBtn = $("#left-btn-slide");
    var rightBtn = $("#right-btn-slide");
    var menu = $("#menu");

    var opened   = false;
    var openedBy = 'left';

    $(".btn-slide").click(function(){
        if (opened) {
            if (openedBy === 'left') {
                menu.animate({
                    left: -menu.width() + 'px'
                }, {
                    duration: 500,
                    complete: function() {
                        menu.addClass('hidden');
                        opened = false;
                    }
                });
            } else if (openedBy === 'right') {
                menu.animate({
                    right: -menu.width() + 'px'
                }, {
                    duration: 500,
                    complete: function() {
                        menu.addClass('hidden');
                        opened = false;
                    }
                });
            }
            leftBtn.animate({
                left: '0px',
            }, {
                duration: 500
            });
            rightBtn.animate({
                right: '0px',
            }, {
                duration: 500
            });
        }
    });
    
    leftBtn.click(function(){
        if (!opened) {
            opened = true;
            openedBy = 'left';

            menu.removeClass('hidden');
            menu.css({
                'left': -menu.width() + 'px',
                'right': 'auto'
            });

            menu.animate({
                left: '0px'
            }, {
                duration: 500
            });
            leftBtn.animate({
                left: menu.width() + 3 + 'px',
            }, {
                duration: 500
            });
        }
    });
    
    rightBtn.click(function(){
        if (!opened) {
            opened = true;
            openedBy = 'right';

            menu.removeClass('hidden');
            menu.css({
                'right': -menu.width() + 'px',
                'left': 'auto'
            });

            menu.animate({
                right: '0px'
            }, {
                duration: 500
            });
            rightBtn.animate({
                right: menu.width() + 3 + 'px',
            }, {
                duration: 500
            });
        }
    });
});