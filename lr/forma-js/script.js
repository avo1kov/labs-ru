let captchaFlag = false,
    cardFlag = false,
    phoneFlag = false,
    doublePassFlag = false,
    passFlag = false,
    emailFlag = false,
    surnameFlag = false,
    nameFlag = false;

const captcha = [
    ['src/1.jpg', 'А301б'],
    ['src/2.jpg', '36 группа'],
    ['src/3.jpg', 'ФИИТ'],
    ['src/4.jpg', 'ФКТиПМ'],
    ['src/5.jpg', 'КубГУ'],
];

const $captchaImg = document.getElementById('captcha-img'),
    $captchaInput = document.getElementById('captcha-input'),
    $captchaMistakes = document.getElementById('captcha-mistakes');

const captchaId = Math.floor(captcha.length * Math.random());
$captchaImg.src = captcha[captchaId][0];

$captchaInput.addEventListener('input', () => {
    $captchaInput.classList.remove('verified');
    $captchaInput.classList.remove('wrong');
    $captchaMistakes.innerText = '';
    captchaFlag = false;
    formVerifying();
});
$captchaInput.addEventListener('keyup', () => {
    if ($captchaInput.value === captcha[captchaId][1]) {
        $captchaInput.classList.add('verified');
        captchaFlag = true;
    } else {
        $captchaMistakes.innerText = 'Надпись переписана с ошибками';
        $captchaInput.classList.add('wrong');
    }
    formVerifying();
});


const $cardInput = document.getElementById('card-input'),
    $cardMistakes = document.getElementById('card-mistakes');

$cardInput.addEventListener('input', () => {
    $cardInput.classList.remove('verified');
    $cardInput.classList.remove('wrong');
    $cardMistakes.innerText = '';
    cardFlag = false;
    formVerifying();
});
$cardInput.addEventListener('blur', () => {
    if (/^(\d{4}[- ]?){3}\d{4}$/.test($cardInput.value)) {
        $cardInput.classList.add('verified');
        cardFlag = true;
    } else {
        $cardInput.classList.add('wrong');
        $cardMistakes.innerText = 'Номер карты должен содержать 16 цифр';
    }
    formVerifying();
});


const $phoneInput = document.getElementById('phone-input'),
    $phoneMistakes = document.getElementById('phone-mistakes');

$phoneInput.addEventListener('input', () => {
    $phoneInput.classList.remove('verified');
    $phoneInput.classList.remove('wrong');
    $phoneMistakes.innerText = '';
    phoneFlag = false;
    formVerifying();
});
$phoneInput.addEventListener('blur', () => {
    if (/^(\+7|8).*$/.test($phoneInput.value)) {
        if (/^(\+7|8)\d{10}$/.test($phoneInput.value)) {
            $phoneInput.classList.add('verified');
            phoneFlag = true;
        } else {
            $phoneInput.classList.add('wrong');
            $phoneMistakes.innerText = 'Номер телефона должен содержать 11 цифр';
        }
    } else {
        $phoneInput.classList.add('wrong');
        $phoneMistakes.innerText = 'Номер телефона должен начинаться с +7 или 8';
    }
    formVerifying();
});


const $doublePasswordInput = document.getElementById('double-password-input'),
    $doublePasswordMistakes = document.getElementById('double-password-mistakes');

$doublePasswordInput.addEventListener('input', () => {
    $doublePasswordInput.classList.remove('verified');
    $doublePasswordInput.classList.remove('wrong');
    $doublePasswordMistakes.innerText = '';
    doublePassFlag = false;
    formVerifying();
});
$doublePasswordInput.addEventListener('blur', () => {
    if ($doublePasswordInput.value === $passwordInput.value && passFlag) {
        $doublePasswordInput.classList.add('verified');
        doublePassFlag = true;
    } else {
        $doublePasswordInput.classList.add('wrong');
        $doublePasswordMistakes.innerText = 'Пароли должны совпадать';
    }
    formVerifying();
});


const $passwordInput = document.getElementById('password-input'),
    $passwordMistakes = document.getElementById('password-mistakes');

$passwordInput.addEventListener('input', () => {
    $passwordInput.classList.remove('verified');
    $passwordInput.classList.remove('wrong');
    $doublePasswordInput.classList.remove('verified');
    $doublePasswordInput.classList.remove('wrong');
    $passwordMistakes.innerText = '';
    $doublePasswordMistakes.innerText = '';
    passFlag = false;
    formVerifying();
});
$passwordInput.addEventListener('blur', () => {
    if (/^[\w!@#$%^*]+$/.test($passwordInput.value)) {
        if ($passwordInput.value.length >= 8) {
            $passwordInput.classList.add('verified');
            passFlag = true;
        } else {
            $passwordInput.classList.add('wrong');
            $passwordMistakes.innerText = 'Пароль должен состоять из не менее 8-ми символов';
        }
    } else {
        $passwordInput.classList.add('wrong');
        $passwordMistakes.innerText = 'Пароль должен содержать только символы 0-9, a-z, A-Z и !@#$%^*_';
    }
    formVerifying();
});


const $emailInput = document.getElementById('email-input'),
    $emailMistakes = document.getElementById('email-mistakes');

$emailInput.addEventListener('input', () => {
    $emailInput.classList.remove('verified');
    $emailInput.classList.remove('wrong');
    $emailMistakes.innerText = '';
    emailFlag = false;
    formVerifying();
});
$emailInput.addEventListener('blur', () => {
    if (/^([\w.]){2,}@([\w]+\.)+[a-zA-Z]{2,}$/.test($emailInput.value)) {
        $emailInput.classList.add('verified');
        emailFlag = true;
    } else {
        $emailInput.classList.add('wrong');
        $emailMistakes.innerText = 'Некорректный e-mail. Пример: login@mail.ru';
    }
    formVerifying();
});


const $surnameInput = document.getElementById('surname-input'),
    $surnameMistakes = document.getElementById('surname-mistakes');

$surnameInput.addEventListener('input', () => {
    $surnameInput.classList.remove('verified');
    $surnameInput.classList.remove('wrong');
    $surnameMistakes.innerText = '';
    surnameFlag = false;
    formVerifying();
});
$surnameInput.addEventListener('blur', () => {
    if (/^[а-яА-Я]{2,}(-[а-яА-Я]+)?$/.test($surnameInput.value)) {
        $surnameInput.classList.add('verified');
        surnameFlag = true;
    } else {
        $surnameInput.classList.add('wrong');
        $surnameMistakes.innerText = 'Фимилия должна состоять из символов а-я и А-Я. Двойная фамилия может быть разделена символом "-"';
    }
    formVerifying();
});


const $nameInput = document.getElementById('name-input'),
    $nameMistakes = document.getElementById('name-mistakes');

$nameInput.addEventListener('input', () => {
    $nameInput.classList.remove('verified');
    $nameInput.classList.remove('wrong');
    $nameMistakes.innerText = '';
    nameFlag = false;
    formVerifying();
});
$nameInput.addEventListener('blur', () => {
    if (/^[а-яА-Я]{3,20}$/.test($nameInput.value)) {
        $nameInput.classList.add('verified');
        nameFlag = true;
    } else {
        $nameInput.classList.add('wrong');
        $nameMistakes.innerText = 'Имя должно состоять из символов а-я и А-Я';
    }
    formVerifying();
});

const submitBtn = document.getElementById('submit-btn');
formVerifying = () => {
    if (captchaFlag && cardFlag && phoneFlag && doublePassFlag && passFlag && emailFlag && surnameFlag && nameFlag) {
        submitBtn.removeAttribute('disabled')
    } else {
        submitBtn.setAttribute('disabled', 'true');
    }
};
