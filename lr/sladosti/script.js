const sweets = [
    'Мишка на севере',
    'Коровка',
    'Птичье молоко',
    'Ириски',
    'Красный мак'
];

let numberOfClickedItems = 0;

const $menuItems = document.getElementById('menu-items');
for (let i = 0; i < sweets.length; i++) {
    const $newSweet = document.createElement('div');
    $newSweet.classList.add('sweet');
    $newSweet.innerText = sweets[i];
    $newSweet.addEventListener('click', (e) => {
        console.log($newSweet.innerText);
        if (!$newSweet.classList.contains('hidden')) {
            numberOfClickedItems++;
            console.log(numberOfClickedItems);
        }
        $newSweet.classList.add('hidden');
        setTimeout(() => {
            if (numberOfClickedItems === sweets.length) {
                $menuItems.classList.add('game-over');
            }
        }, 1000);
    });
    $menuItems.appendChild($newSweet);
}

const $outside = document.getElementById('outside');
$outside.addEventListener('click', () => {
    $menuItems.classList.toggle('hidden');
    $outside.classList.toggle('opened');
});