const $ol = document.getElementById('ol');

newPrompt = () => {
    const response = prompt();
    if (response) {
        const $li = document.createElement('li');
        $li.innerText = response;
        $ol.appendChild($li);

        setTimeout(() => {
            newPrompt();
        }, 1);
    }
};

newPrompt();