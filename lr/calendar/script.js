const weekdays = ["ВС", "ПН", "ВТ", "CР", "ЧТ", "ПТ", "СБ"];
const months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

current_date = new Date();
wday = current_date.getUTCDay();
year = current_date.getFullYear();
day = current_date.getUTCDate();
month = current_date.getUTCMonth();
minutes = current_date.getMinutes();
seconds = current_date.getSeconds();
hours = current_date.getHours();
ampm = hours >= 12 ? 'pm' : 'a  m';

hours = hours % 12;
hours = hours ? hours : 12;
hours = hours < 10 ? '0' + hours : hours;
minutes = minutes < 10 ? '0' + minutes : minutes;
seconds = seconds < 10 ? '0' + seconds : seconds;
day = day < 10 ? '0' + day : day;

date = weekdays[wday] + ', ' + year + ' ' + day + ' ' + months[month];
time = hours + '-' + minutes + '-' + seconds + ' (' + ampm + ')';
console.log('Дата: ' + date);
console.log('Время: ' + time);


timer = document.getElementById('time');
timer.innerHTML = weekdays[wday] + ', ' + year + ' ' + day + ' ' + months[month] + ', ' + hours + '-' + minutes + '-' + seconds + ' (' + ampm + ')';




setInterval(function () {
    current_date = new Date();
    wday = current_date.getUTCDay();
    year = current_date.getFullYear();
    day = current_date.getUTCDate();
    month = current_date.getUTCMonth();
    day = day < 10 ? '0' + day : day;
    seconds = current_date.getSeconds();
    hours = current_date.getHours();
    minutes = current_date.getMinutes();
    ampm = hours >= 12 ? 'pm' : 'a  m';
    hours = hours % 12;
    hours = hours ? hours : 12;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    timer.innerHTML = weekdays[wday] + ', ' + year + ' ' + day + ' ' + months[month] + ', ' + hours + '-' + minutes + '-' + seconds + ' (' + ampm + ')';
}, delay = 1000);


function Calendar(year, mon) {
    var elem = document.getElementById("calendar");
    var d = new Date(year, mon);
    var curr_day = mon == month ? Number(day) : -1;

    var table = '<table><tr><th>пн</th><th>вт</th><th>ср</th><th>чт</th><th>пт</th><th class="wekeends">сб</th><th class="wekeends">вс</th></tr><tr>';


    for (var i = 0; i < getDay(d); i++) {
        table += '<td class = "empty"></td>';
    }

    while (d.getMonth() == mon) {
        if ((getDay(d) % 7 == 5) || (getDay(d) % 7 == 6)) {
            if (d.getDate() != curr_day)
                table += '<td class = "weekdays">' + d.getDate() + '</td>';
            else table += '<td class = "current_day">' + d.getDate() + '</td>';
        } else
        if (d.getDate() != curr_day) {
            table += '<td>' + d.getDate() + '</td>';
        } else {
            table += '<td class = "current_day">' + d.getDate() + '</td>';
        }
        if (getDay(d) % 7 == 6) {
            table += '</tr><tr>';
        }
        d.setDate(d.getDate() + 1);
    }

    if (getDay(d) != 0) {
        for (i = getDay(d); i < 7; i++) {
            table += '<td class = "empty"></td>';
        }
    }

    table += '</tr></table>';
    elem.innerHTML = table;
    var m = document.getElementById("month");
    m.innerHTML = months[mon]
}

function getDay(date) {
    var day = date.getDay();
    if (day == 0) day = 7;
    return day - 1;
}

Calendar(year, month)
