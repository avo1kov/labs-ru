<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Форма</title>
    <link rel="stylesheet" href="css_php.css">
</head>

<body>
    <main>
        <h3>Проверьте правильность введённых данных:</h3>
        <? 
            $f_name = $_POST['first_name'];
            $s_name = $_POST['second_name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $card = $_POST['card'];
        ?>
        <table>
            <tr>
                <td>Вас зовут:</td>
                <td><? echo $f_name,' ', $s_name ?></td>
            </tr>            
            <tr>
                <td>Ваш email:</td>
                <td><? echo $email ?></td>
            </tr>            
            <tr>
                <td>Телефон:</td>
                <td><? echo $phone ?></td>
            </tr>            
            <tr>
                <td>Номер карты:</td>
                <td><? echo $card ?></td>
            </tr>
            
        </table>
        <form action="reg.php" method="POST">
           <input type="text" value="<? echo $f_name, ' ', $s_name ?>" name = "full_name" hidden>
           <input type="text" value="<? echo $email ?>" name = "email" hidden>
           <input type="submit" value="Всё верно">
           <input type="button" value="Изменить данные" OnClick="history.back()">
        </form>   
            
    </main>
    <a href="./index.html" class="back">← Назад</a>
</body>

</html>
