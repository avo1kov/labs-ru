<?
session_start();
$database = array(
    "admin" => "qwerty",
    "user" => "12345",
    "nk29" => "NsnK",
    "root" => "toor");
    $login = $_POST['login'];
    $pass = $_POST['pass'];

     if(!isset($_SESSION['count_of_try'])){
          $_SESSION['count_of_try'] = 0;
     }


     if ($_SESSION['count_of_try'] > 3) {
        header("Location: index.php");
        exit;
     }
    
     if ($_SESSION['count_of_try'] == 3) {
         $_SESSION['time'] = date("H:i:s");
     }

     if (array_key_exists($login, $database)){
         if ($database[$login] == $pass){
            $_SESSION['username'] = $_POST['login'];
            unset($_SESSION['count_of_try']);
            header("Location: secretplace.php");
            exit; 
         }
     }
    $_SESSION['count_of_try'] += 1;
?>

<html>

<head>
    <title>Ошибка</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="style.css">
</head>

<script type="text/javascript">
    setTimeout('location.replace("index.php")', 1000);
</script>

<body>
    <p>Вы ввели неверное имя пользователя/пароль
        <? echo $_SESSION['count_of_try'].' раз' ?>
    </p>
</body>

</html>
