<? 
session_start();

if (isset($_SESSION['username'])){
        header("Location: secretplace.php");
    }


if (isset($_SESSION['time'])) {
    $current = strtotime(date("H:i:s"));
    if ($current - strtotime($_SESSION['time'])< 5) {
        echo '<p style = "text-align: center">Превышено допустимое количество попыток</p>';
        exit; 
    }
    unset($_SESSION['count_of_try']);
    unset($_SESSION['time']);
}
?>

<html>

<head>
    <meta charset="UTF-8">
    <title>Авторизация</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <form action="authorize.php" method="POST">
        <div class="inputs">
            <label for="login">Логин:</label>
            <input type="text" name="login">

            <label for="pass">Пароль:</label>
            <input type="password" name="pass">

        </div>
        <input type="submit" value="Готово">
    </form>
    <a href="../../index.html#labs" class="back">← Назад</a>
</body>

</html>
